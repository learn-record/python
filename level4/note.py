# 模块动态导入

# lib目录下的aapython


__import__()  是解释器内部用的

lib = __import__('lib.aa')  lib 相当于导入了lib

obj = mod.aa.C()
print(obj.name)

官方建议使用 importlib

import importlib

aa = importlib.import_module('lib.aa')  这里直接相当于导入了aa



class Foo(object):
    def __call__(self, *args, **kwargs):
        print('call')

Foo() 实例化
Foo()() 执行 __call__ 方法

__metaclass__ 用来定义这个类 以怎样的形式被创建


try :
    pass
except (xerr,xxerr) as e  3.x
except (xerr,xxerr) ， e   2.7

else:   没有发生异常就执行

finally: 无论如何都执行

raise:valueError 手动触发异常

exctpt Exception as e : 放在异常处理最后面


# 断言
assert  1 = 2

接下来的程序运行 依赖于上面的判断


# socket



import socket

family address 地址簇

    AF.INET ipv4
    AF.INET6
    AF.UNIX
socket protocol type
    sock.SOCK_STREAM tcp/ip
    socket.SOCK_DGRAM UDP

服务端
server = socket.socket(AF.INET,sock.SOCK_STREAM)

server.bind(localhost,9999)
server.listen()

while True:
    conn,addr = server.accept() #阻塞
    while True:
        print('new conn' , addr)
        data = conn.recv(1024) # 官方建议不超过8k  8192 #recv 默认是阻塞的 等待客户端
        if not data:
            break

        print(data)
        conn.send(data.upper())

        #客户端已断开 conn.recv 收到的数据就都是空数据

客户端

client = socket.socket()
client.connect(serverip,9999)
client.send(data)
client.rect(data)



简单ssh  服务端
import socket
import time
server = socket.socket()
server.bind(('localhjost',9999))

server.listen()
while True:
    conn,addr = server.accept()
    print('new addr ' ,addr)
    while True:

        data = conn.recv(1024)
        if not data :
            print('客户端已断开')
            break
        print('执行指令：',data)
        cmd_res = ps.popen(data.decode("utf-8")).read()
        if len(cmd_res) ==0 :
            cmd_res='cmd has no data'
        conn.send(str(len(cmd_res.encode())).encode("utf-8"))
        # time.sleep(0.5)      来避免粘包   可以避免 方法比较low
        client_ack = conn.recv(1024)
        conn.send(cmd_res.encode("utf-8"))

conn.close()
简单ssh  客户端
client = socket.socket()

client.connect(('localhost',9999))

while True:
    cmd = input('>>:'.strip())
    if len(cmd) == 0:continue
    client.send(cmd.encode("utf-8"))
    cmd_res = client.recv(1024)
    print(cmd_res.decode("utf-8"))




# socketserver


--常用
socketserver.TCPServer()
socketserver.UDPServer()
--不常用 服务器内部通信
socketserver.UnixStreamServer()
socketserver.UnixDatagramServer()


创建一个sockerServer


import socketserver

class NyTCPhandle(socketserver,BaseException)




python 多线程不适合cpu密集型任务 适合 io密集型任务

#python  多进程

import multiprocessing


