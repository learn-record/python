


import threading

import queue



class MyThread(threading.Thread):

    def __init__(self,n):

        super(MyThread,self).__init__()
        self.n=n

    def run(self):
        print(self.n)


for i in range(100):
    t = MyThread(i)
    t.start()




