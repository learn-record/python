#!/usr/bin/python3
# -*- coding:utf-8 -*-
# @---wufeng---

import os, sys, subprocess
import yaml

runenv = 'uat'
base_path = '/data/docker-run'
service_release_info_url = 'http://cf.touchfound.net/docker-config-file/docker-compose/' + runenv + '/service_release_info.yml'
service_release_config_file = base_path + '/service_release_info.yml'
app_compose_file = base_path + '/app.yml'
log_compose_file = base_path + '/log.yml'


def execute_command(compose_config, service_name, command_type):
    if command_type == 'start':
        _command = ['/usr/bin/docker-compose', '-f', compose_config[0], 'up', '-d', service_name]
    elif command_type == 'rm':
        _command = ['/usr/bin/docker-compose', '-f', compose_config[0], 'rm', '-f', service_name]
    elif command_type == 'logs':
        _command = ['/usr/bin/docker-compose', '-f', compose_config[0], 'logs', '-f', service_name]
    elif command_type == 'reload':
        subprocess.check_call("docker login --username=新众聚联 -p 123.com registry.cn-beijing.aliyuncs.com", shell=True)
        subprocess.check_call(['/usr/bin/docker', 'pull', images_info(compose_config, service_name)[0]])
        execute_command(compose_config, service_name, 'kill')
        execute_command(compose_config, service_name, 'rm')
        execute_command(compose_config, service_name, 'start')
        exit(0)
    else:
        _command = ['/usr/bin/docker-compose', '-f', compose_config[0], command_type, service_name]

    try:
        subprocess.check_call(['cd', base_path])
        subprocess.check_call(_command)
        _command[-1] = service_name + '-log'
        _command[2] = compose_config[1]
        subprocess.check_call(_command)
    except subprocess.CalledProcessError as err:
        print("Command Error", err)
        exit(1)


def compose_start_all(compose_config, service_name):
    # todo
    pass


def compose_stop_all(compose_config, service_name):
    # todo
    pass


def compose_reload_all(compose_config, service_name):
    # todo
    pass


def compose_ps_all(compose_config, service_name):
    # todo
    pass


def compose_list(compose_config):
    service_map = yml_file_handle(compose_config[0])['services']
    for service_name in service_map:
        version = service_map[service_name]['image'].split(':')[-1]
        print('%-35s \t %-20s' % ('service name: ' + service_name, 'version: ' + version))


def images_clean():
    try:
        subprocess.check_call('docker rmi $(docker images -f "dangling=true" -q)', shell=True)
    except subprocess.CalledProcessError as err:
        print("Command Error", err)
        exit(1)


def images_info(compose_config, service_name):
    config_map = yml_file_handle(compose_config[0])

    images_info = str(config_map['services'][service_name]['image']).split('/')[-1]

    if ':' in images_info:
        images_name = images_info.split(':')[0]
        images_ver = images_info.split(':')[-1]
    else:
        images_name = images_info
        images_ver = 'latest'
    images_url = 'registry.cn-beijing.aliyuncs.com/touchfound/' + images_name + ':' + images_ver
    return images_url, images_name, images_ver,


def yml_file_handle(file_path):
    try:
        f = open(file_path, "r", encoding="utf-8")
        config_map = yaml.load(f)
        f.close()
        return config_map

    except IOError as err:
        print('config file read error please check %s' % (file_path))
        exit(1)


def service_type_judge(compose_config, service_name):
    if service_name not in yml_file_handle(compose_config[0])['services']:
        print('input service name %s not in docker-compose.yml file' % service_name)
        exit(1)


def make_app_compose_file(config_map):
    compose_map = {'version': '2'}
    networks_dist = {'kdx-network-0': {'driver': 'bridge'}}
    instances_map = {}
    service_map = config_map['services']
    if config_map['runenv'] == 'pro':
        runenv = 'pro'
        version_release = '-release'
    else:
        runenv = 'uat'
        version_release = '-beta'

    for service_name in service_map:
        config_file_url_base = 'http://cf.touchfound.net/docker-config-file/' + service_name + '/' + runenv + '/'
        local_log_path = '/data/' + service_name + '/logs'
        image_url = 'registry.cn-beijing.aliyuncs.com/touchfound/' + service_name + ':' + str(service_map[service_name][
                                                                                                  'version']) + version_release
        instances_map[service_name] = {'image': image_url,
                                       'volumes': [local_log_path + ':' + service_map[service_name]['log_path']],
                                       'environment': ['spring.cloud.config.profile=' + runenv,
                                                       'APPNAME=' + service_name, 'RUNENV=' + runenv],
                                       'networks': ['kdx-network-0']
                                       }
        if 'ports' in service_map[service_name].keys():
            instances_map[service_name]['ports'] = service_map[service_name]['ports']

        if 'config_file' in service_map[service_name].keys():
            instances_map[service_name]['environment'] = instances_map[service_name][
                                                             'environment'] + ['SETTING_URL=' + config_file_url_base + \
                                                                               service_map[service_name]['config_file']]

    compose_map['services'] = instances_map
    compose_map['networks'] = networks_dist
    f = open(app_compose_file, "w", encoding="utf-8")
    yaml.dump(compose_map, f)
    f.close()
    return os.path.abspath(app_compose_file)


def make_log_compose_file(config_map):
    compose_map = {'version': '2'}
    networks_dist = {'kdx-network-0': {'driver': 'bridge'}}
    instances_map = {}
    service_map = config_map['services']
    if config_map['runenv'] == 'pro':
        runenv = 'pro'
    else:
        runenv = 'uat'
    image_url = 'registry.cn-beijing.aliyuncs.com/touchfound/filebeat'

    for service_name in service_map:
        local_log_path = '/data/' + service_name + '/logs'
        instances_map[service_name + '-log'] = {'image': image_url,
                                                'volumes': [local_log_path + ':' + '/logfile'],
                                                'environment': [
                                                    'APPNAME=' + service_name, 'RUNENV=' + runenv, 'MUL=N'],
                                                'networks': ['kdx-network-0']
                                                }

    compose_map['services'] = instances_map
    compose_map['networks'] = networks_dist
    f = open(log_compose_file, "w", encoding="utf-8")
    yaml.dump(compose_map, f)
    f.close()
    return os.path.abspath(log_compose_file)


def get_config_file():
    try:
        subprocess.check_call(['/usr/bin/wget', '-O', service_release_config_file, '-o', sys.argv[0] + '.log',
                               service_release_info_url])
    except subprocess.CalledProcessError as err:
        print("Command Error", err)
        exit(1)
    compose_config = (make_app_compose_file(yml_file_handle(service_release_config_file)),
                      make_log_compose_file(yml_file_handle(service_release_config_file)))

    return compose_config


if __name__ == '__main__':

    if not os.path.isdir(base_path):
        os.mkdir(base_path)

    if len(sys.argv) == 3:
        compose_config = get_config_file()
        service_name = sys.argv[2]

        service_type_judge(compose_config, service_name)

        if sys.argv[1] in ('start', 'stop', 'restart', 'reload', 'ps', 'logs'):
            execute_command(compose_config, service_name, sys.argv[1])
        else:
            print('please input start|stop|restart|ps|reload|logs')

    elif len(sys.argv) == 2 and sys.argv[1] in ('startall', 'psall', 'stopall', 'reloadall', 'list'):
        compose_config = get_config_file()
        if sys.argv[1] == 'startall':
            compose_start_all(compose_config)
        elif sys.argv[1] == 'stoptall':
            compose_stop_all(compose_config)
        elif sys.argv[1] == 'psall':
            compose_ps_all(compose_config)
        elif sys.argv[1] == 'reloadall':
            compose_reload_all(compose_config)
        elif sys.argv[1] == 'list':
            compose_list(compose_config)
        else:
            print('please input start|stop|restart|status|reload')
    else:

        print('please input start|stop|restart|status|reload and service name!~')
