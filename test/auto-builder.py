#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Author: wufeng

import os, sys, logging, shutil
import subprocess

logging.basicConfig(filename="auto_build.log",  # 日志记录 模块配置
                    format='[%(asctime)s] %(levelname)s: %(message)s',
                    level="DEBUG")
log = logging

GIT_BASE = 'http://10.8.4.156'
BUILD_BASE = '/tmp/auto_build'


def buildImages(code_path):
    return


def pushAliyun(images_num, project_name, docker_image_version):
    return


def getCode(group_name, project_name, branch_name='master'):
    from gittle import Gittle,GittleAuth

    #key_file = open('id_rsa')
    #auth = GittleAuth(pkey=key_file)
    auth = GittleAuth(username="wufeng", password="q1w2e3r4")

    repo_path = BUILD_BASE + '/' + project_name
    repo_url = GIT_BASE + '/' + group_name + '/' + project_name + '.git'
    print(repo_url)

    if os.path.isdir(repo_path):
        shutil.rmtree(repo_path)
    elif not os.path.isdir(BUILD_BASE):
        os.mkdir(BUILD_BASE)

    repo=Gittle.clone(repo_url, repo_path, auth=auth)
    if branch_name != 'master':
        repo.switch_branch(branch_name)
    return repo_path

def get_code_info(code_path):
    return


if __name__ == '__main__':

    # if len(sys.argv) not in (3, 4):
    #     print("input yout group_name,project_name and branch_name")
    #     log.warn('Input parameter is not enough')
    #     exit(1)
    # elif len(sys.argv) == 3:
    #
    #     group_name, project_name = sys.argv[1 - 3]
    #     build_images(group_name, project_name)
    #
    #     push_aliyun(images_num, project_name, docker_image_version)

    getCode('open-api','open-api-server','master')
