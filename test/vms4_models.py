# coding: utf-8
from sqlalchemy import Column, DateTime, Index, Numeric, String, Table, Text, text
from sqlalchemy.dialects.oracle.base import RAW
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


t_sys_export_schema_01 = Table(
    'sys_export_schema_01', metadata,
    Column('abort_step', Numeric(asdecimal=False)),
    Column('access_method', String(16)),
    Column('ancestor_object_name', String(128)),
    Column('ancestor_object_schema', String(128)),
    Column('ancestor_object_type', String(128)),
    Column('ancestor_process_order', Numeric(asdecimal=False)),
    Column('base_object_name', String(128)),
    Column('base_object_schema', String(128)),
    Column('base_object_type', String(128)),
    Column('base_process_order', Numeric(asdecimal=False), index=True),
    Column('block_size', Numeric(asdecimal=False)),
    Column('cluster_ok', Numeric(asdecimal=False)),
    Column('completed_bytes', Numeric(asdecimal=False)),
    Column('completed_rows', Numeric(asdecimal=False)),
    Column('completion_time', DateTime),
    Column('control_queue', String(128)),
    Column('creation_level', Numeric(asdecimal=False)),
    Column('creation_time', DateTime),
    Column('cumulative_time', Numeric(asdecimal=False)),
    Column('data_buffer_size', Numeric(asdecimal=False)),
    Column('data_io', Numeric(asdecimal=False)),
    Column('dataobj_num', Numeric(asdecimal=False)),
    Column('db_version', String(60)),
    Column('degree', Numeric(asdecimal=False)),
    Column('domain_process_order', Numeric(asdecimal=False)),
    Column('dump_allocation', Numeric(asdecimal=False)),
    Column('dump_fileid', Numeric(asdecimal=False)),
    Column('dump_length', Numeric(asdecimal=False)),
    Column('dump_orig_length', Numeric(asdecimal=False)),
    Column('dump_position', Numeric(asdecimal=False)),
    Column('duplicate', Numeric(asdecimal=False)),
    Column('elapsed_time', Numeric(asdecimal=False)),
    Column('error_count', Numeric(asdecimal=False)),
    Column('extend_size', Numeric(asdecimal=False)),
    Column('file_max_size', Numeric(asdecimal=False)),
    Column('file_name', String(4000)),
    Column('file_type', Numeric(asdecimal=False)),
    Column('flags', Numeric(asdecimal=False)),
    Column('grantor', String(128)),
    Column('granules', Numeric(asdecimal=False)),
    Column('guid', RAW),
    Column('in_progress', String(1)),
    Column('instance', String(60)),
    Column('instance_id', Numeric(asdecimal=False)),
    Column('is_default', Numeric(asdecimal=False)),
    Column('job_mode', String(21)),
    Column('job_version', String(60)),
    Column('last_file', Numeric(asdecimal=False)),
    Column('last_update', DateTime),
    Column('load_method', Numeric(asdecimal=False)),
    Column('metadata_buffer_size', Numeric(asdecimal=False)),
    Column('metadata_io', Numeric(asdecimal=False)),
    Column('name', String(128)),
    Column('object_int_oid', String(130)),
    Column('object_long_name', String(4000)),
    Column('object_name', String(200)),
    Column('object_number', Numeric(asdecimal=False)),
    Column('object_path_seqno', Numeric(asdecimal=False)),
    Column('object_row', Numeric(asdecimal=False)),
    Column('object_schema', String(128)),
    Column('object_tablespace', String(128)),
    Column('object_type', String(128)),
    Column('object_type_path', String(200)),
    Column('objnum', Numeric(asdecimal=False)),
    Column('old_value', String(4000)),
    Column('operation', String(8)),
    Column('option_tag', String(128)),
    Column('orig_base_object_name', String(128)),
    Column('orig_base_object_schema', String(128)),
    Column('original_object_name', String(128)),
    Column('original_object_schema', String(128)),
    Column('packet_number', Numeric(asdecimal=False)),
    Column('parallelization', Numeric(asdecimal=False)),
    Column('parent_object_name', String(128)),
    Column('parent_object_schema', String(128)),
    Column('parent_process_order', Numeric(asdecimal=False)),
    Column('partition_name', String(128)),
    Column('phase', Numeric(asdecimal=False)),
    Column('platform', String(101)),
    Column('process_name', String(128)),
    Column('process_order', Numeric(asdecimal=False)),
    Column('processing_state', String(1)),
    Column('processing_status', String(1)),
    Column('property', Numeric(asdecimal=False)),
    Column('proxy_schema', String(128)),
    Column('proxy_view', String(128)),
    Column('queue_tabnum', Numeric(asdecimal=False)),
    Column('remote_link', String(128)),
    Column('scn', Numeric(asdecimal=False)),
    Column('seed', Numeric(asdecimal=False), index=True),
    Column('service_name', String(64)),
    Column('size_estimate', Numeric(asdecimal=False)),
    Column('src_compat', String(60)),
    Column('start_time', DateTime),
    Column('state', String(12)),
    Column('status_queue', String(128)),
    Column('subpartition_name', String(128)),
    Column('target_xml_clob', Text),
    Column('tde_rewrapped_key', RAW),
    Column('template_table', String(128)),
    Column('timezone', String(64)),
    Column('total_bytes', Numeric(asdecimal=False)),
    Column('trigflag', Numeric(asdecimal=False)),
    Column('unload_method', Numeric(asdecimal=False)),
    Column('user_directory', String(4000)),
    Column('user_file_name', String(4000)),
    Column('user_name', String(128)),
    Column('value_n', Numeric(asdecimal=False)),
    Column('value_t', String(4000)),
    Column('version', Numeric(asdecimal=False)),
    Column('work_item', String(21)),
    Column('xml_clob', Text),
    Column('xml_process_order', Numeric(asdecimal=False)),
    Index('sys_mtable_000025670_ind_4', 'original_object_schema', 'original_object_name', 'partition_name'),
    Index('sys_mtable_000025670_ind_1', 'object_schema', 'original_object_name', 'object_type'),
    Index('sys_mtable_000025670_ind_2', 'object_schema', 'object_name', 'object_type', 'partition_name', 'subpartition_name'),
    Index('sys_c0049660', 'process_order', 'duplicate', unique=True),
    schema='vms4u4'
)


class TBkOrder(Base):
    __tablename__ = 't_bk_order'
    __table_args__ = {'schema': 'vms4u4'}

    cost_id = Column(Numeric(scale=0, asdecimal=False))
    create_time = Column(DateTime)
    order_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    seq = Column(String(50))
    total_amount = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    wechat_package = Column(String(50))
    wechat_user_id = Column(Numeric(scale=0, asdecimal=False))


class TBkOrderCost(Base):
    __tablename__ = 't_bk_order_cost'
    __table_args__ = {'schema': 'vms4u4'}

    cost_amount = Column(Numeric(scale=0, asdecimal=False))
    cost_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    cost_status = Column(Numeric(scale=0, asdecimal=False))
    cost_time = Column(DateTime)
    cost_type = Column(Numeric(scale=0, asdecimal=False))
    deal_seq = Column(String(50))


class TBkOrderGood(Base):
    __tablename__ = 't_bk_order_good'
    __table_args__ = {'schema': 'vms4u4'}

    base_price = Column(Numeric(scale=0, asdecimal=False))
    deposit_id = Column(Numeric(scale=0, asdecimal=False))
    extract_code = Column(String(8))
    good_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    notify_id = Column(Numeric(scale=0, asdecimal=False))
    order_id = Column(Numeric(scale=0, asdecimal=False))
    real_price = Column(Numeric(scale=0, asdecimal=False))
    refund_id = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    status = Column(Numeric(scale=0, asdecimal=False))
    valid_day = Column(String(10))
    vendout_id = Column(Numeric(scale=0, asdecimal=False))
    vendout_status = Column(Numeric(scale=0, asdecimal=False))
    notify_status = Column(Numeric(scale=0, asdecimal=False))


class TBkOrderGoodDeposit(Base):
    __tablename__ = 't_bk_order_good_deposit'
    __table_args__ = {'schema': 'vms4u4'}

    deposit_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    deposit_price = Column(Numeric(scale=0, asdecimal=False))
    refund_id = Column(Numeric(scale=0, asdecimal=False))


class TBkOrderGoodNotify(Base):
    __tablename__ = 't_bk_order_good_notify'
    __table_args__ = {'schema': 'vms4u4'}

    notify_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    notify_status = Column(Numeric(scale=0, asdecimal=False))
    notify_time = Column(DateTime)
    order_good_id = Column(Numeric(scale=0, asdecimal=False), unique=True)


class TBkOrderGoodVendout(Base):
    __tablename__ = 't_bk_order_good_vendout'
    __table_args__ = {'schema': 'vms4u4'}

    order_good_id = Column(Numeric(scale=0, asdecimal=False), unique=True)
    vendout_channel_id = Column(Numeric(scale=0, asdecimal=False))
    vendout_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    vendout_status = Column(Numeric(scale=0, asdecimal=False))
    vendout_time = Column(DateTime)


class TBkOrderRefund(Base):
    __tablename__ = 't_bk_order_refund'
    __table_args__ = {'schema': 'vms4u4'}

    order_id = Column(Numeric(scale=0, asdecimal=False))
    refund_amount = Column(Numeric(scale=0, asdecimal=False))
    refund_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    refund_seq = Column(String(50))
    refund_status = Column(Numeric(scale=0, asdecimal=False))
    refund_time = Column(DateTime)
    refund_type = Column(Numeric(scale=0, asdecimal=False))


class TCoupon(Base):
    __tablename__ = 't_coupon'
    __table_args__ = {'schema': 'vms4u4'}

    consignee_id = Column(Numeric(scale=0, asdecimal=False))
    coupon_begin = Column(DateTime)
    coupon_end = Column(DateTime)
    coupon_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    coupon_proportion = Column(Numeric(scale=0, asdecimal=False))
    coupon_reduce_value = Column(Numeric(scale=0, asdecimal=False))
    coupon_satisfy_value = Column(Numeric(scale=0, asdecimal=False))
    coupon_type = Column(Numeric(scale=0, asdecimal=False))
    coupon_value = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)


class TDataOrder(Base):
    __tablename__ = 't_data_order'
    __table_args__ = {'schema': 'vms4u4'}

    data_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    province_name = Column(String(100))
    city_name = Column(String(100))
    area_name = Column(String(100))
    company_name = Column(String(100))
    node_address = Column(String(200))
    business_district_name = Column(String(200))
    mtvm_status_name = Column(String(200))
    order_seq = Column(String(50))
    outer_seq = Column(String(50))
    inner_code = Column(String(20))
    channel_code = Column(String(50))
    vendout_num = Column(String(20))
    sku_type_name = Column(String(200))
    type_name = Column(String(200))
    sku_code = Column(String(200))
    sku_name = Column(String(200))
    cost_type_name = Column(String(200))
    cost_money = Column(String(50))
    card_cost_money = Column(String(50))
    card_rebate = Column(String(50))
    card_money = Column(String(50))
    really_money = Column(String(50))
    pay_time = Column(DateTime)
    vendout_time = Column(DateTime)
    out_status_name = Column(String(50))
    out_status_failreason = Column(String(50))
    refund_info = Column(String(50))
    memo = Column(String(200))
    hardware_code = Column(String(200))
    sku_money = Column(String(20))
    order_type = Column(String(20))
    valid_time = Column(String(20))
    order_status_name = Column(String(20))
    company_id = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    node_id = Column(Numeric(scale=0, asdecimal=False))
    node_name = Column(String(50))
    province_id = Column(Numeric(scale=0, asdecimal=False))
    city_id = Column(Numeric(scale=0, asdecimal=False))
    area_id = Column(Numeric(scale=0, asdecimal=False))
    business_district_id = Column(Numeric(scale=0, asdecimal=False))
    sku_type_id = Column(Numeric(scale=0, asdecimal=False))
    cost_id = Column(Numeric(scale=0, asdecimal=False))
    detail_id = Column(Numeric(scale=0, asdecimal=False), index=True)
    c_time = Column(DateTime)
    u_time = Column(DateTime)
    order_type_cn = Column(String(20))
    pay_type_id = Column(Numeric(scale=0, asdecimal=False))
    order_master_c_time = Column(DateTime)
    notify_time = Column(DateTime)
    type_id = Column(Numeric(scale=0, asdecimal=False))


t_t_data_order_tmp = Table(
    't_data_order_tmp', metadata,
    Column('data_id', Numeric(scale=0, asdecimal=False), nullable=False),
    Column('province_name', String(100)),
    Column('city_name', String(100)),
    Column('area_name', String(100)),
    Column('company_name', String(100)),
    Column('node_address', String(200)),
    Column('business_district_name', String(200)),
    Column('mtvm_status_name', String(200)),
    Column('order_seq', String(50)),
    Column('outer_seq', String(50)),
    Column('inner_code', String(20)),
    Column('channel_code', String(50)),
    Column('vendout_num', String(20)),
    Column('sku_type_name', String(200)),
    Column('type_name', String(200)),
    Column('sku_code', String(200)),
    Column('sku_name', String(200)),
    Column('cost_type_name', String(200)),
    Column('cost_money', String(50)),
    Column('card_cost_money', String(50)),
    Column('card_rebate', String(50)),
    Column('card_money', String(50)),
    Column('really_money', String(18)),
    Column('pay_time', DateTime),
    Column('vendout_time', DateTime),
    Column('out_status_name', String(50)),
    Column('out_status_failreason', String(50)),
    Column('refund_info', String(50)),
    Column('memo', String(200)),
    Column('hardware_code', String(200)),
    Column('sku_money', String(20)),
    Column('order_type', String(20)),
    Column('valid_time', String(20)),
    Column('order_status_name', String(20)),
    Column('company_id', Numeric(scale=0, asdecimal=False)),
    Column('sku_id', Numeric(scale=0, asdecimal=False)),
    Column('node_id', Numeric(scale=0, asdecimal=False)),
    Column('node_name', String(50)),
    Column('province_id', Numeric(scale=0, asdecimal=False)),
    Column('city_id', Numeric(scale=0, asdecimal=False)),
    Column('area_id', Numeric(scale=0, asdecimal=False)),
    Column('business_district_id', Numeric(scale=0, asdecimal=False)),
    Column('sku_type_id', Numeric(scale=0, asdecimal=False)),
    Column('cost_id', Numeric(scale=0, asdecimal=False)),
    Column('detail_id', Numeric(scale=0, asdecimal=False)),
    Column('c_time', DateTime),
    Column('u_time', DateTime),
    Column('order_type_cn', String(20)),
    Column('pay_type_id', Numeric(scale=0, asdecimal=False)),
    Column('order_master_c_time', DateTime),
    Column('notify_time', DateTime),
    Column('type_id', Numeric(scale=0, asdecimal=False)),
    schema='vms4u4'
)


class TErpDataSync(Base):
    __tablename__ = 't_erp_data_sync'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    data_content = Column(String(2000))
    data_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    data_type = Column(String(20))
    sync_mark = Column(Numeric(scale=0, asdecimal=False))
    sync_time = Column(DateTime)
    sync_url = Column(String(200))


class TMallApplyCost(Base):
    __tablename__ = 't_mall_apply_cost'
    __table_args__ = {'schema': 'vms4u4'}

    card_apply_id = Column(Numeric(scale=0, asdecimal=False))
    cost_account = Column(String(50))
    cost_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    cost_outer_seq = Column(String(50))
    cost_price = Column(Numeric(scale=0, asdecimal=False))
    cost_status = Column(Numeric(scale=0, asdecimal=False))
    cost_time = Column(DateTime)
    pay_type = Column(Numeric(scale=0, asdecimal=False))


class TMallApplyMaster(Base):
    __tablename__ = 't_mall_apply_master'
    __table_args__ = {'schema': 'vms4u4'}

    apply_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    apply_time = Column(DateTime)
    available_amount = Column(Numeric(scale=0, asdecimal=False))
    card_number = Column(String(20))
    cost_status = Column(Numeric(scale=0, asdecimal=False))
    end_time = Column(DateTime)
    lock_amount = Column(Numeric(scale=0, asdecimal=False))
    online_card_id = Column(Numeric(scale=0, asdecimal=False))
    pay_amount = Column(Numeric(scale=0, asdecimal=False))
    total_amount = Column(Numeric(scale=0, asdecimal=False))
    used_amount = Column(Numeric(scale=0, asdecimal=False))
    wechat_user_id = Column(Numeric(scale=0, asdecimal=False))


class TMallCardSku(Base):
    __tablename__ = 't_mall_card_sku'
    __table_args__ = {'schema': 'vms4u4'}

    cardid = Column(Numeric(scale=0, asdecimal=False))
    card_sku_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    skuid = Column(Numeric(scale=0, asdecimal=False))


class TMallOnlinecard(Base):
    __tablename__ = 't_mall_onlinecard'
    __table_args__ = {'schema': 'vms4u4'}

    actual_amount = Column(Numeric(scale=0, asdecimal=False), nullable=False)
    cardid = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    card_desc = Column(String(30))
    card_name = Column(String(20), nullable=False)
    card_remark = Column(String(50))
    card_type = Column(Numeric(scale=0, asdecimal=False))
    enddate = Column(DateTime)
    expire = Column(Numeric(scale=0, asdecimal=False), nullable=False)
    limite_count = Column(Numeric(scale=0, asdecimal=False), nullable=False)
    limite_perone = Column(Numeric(scale=0, asdecimal=False))
    spend_amount = Column(Numeric(scale=0, asdecimal=False), nullable=False)
    startdate = Column(DateTime)
    whole_sku = Column(Numeric(scale=0, asdecimal=False))


class TMtArea(Base):
    __tablename__ = 't_mt_area'
    __table_args__ = {'schema': 'vms4u4'}

    area_code = Column(String(10))
    area_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    area_level = Column(Numeric(scale=0, asdecimal=False))
    area_name = Column(String(50))
    p_id = Column(Numeric(scale=0, asdecimal=False))


class TMtBusinessDistrict(Base):
    __tablename__ = 't_mt_business_district'
    __table_args__ = {'schema': 'vms4u4'}

    business_district_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    business_district_name = Column(String(50))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))


class TMtCard(Base):
    __tablename__ = 't_mt_card'
    __table_args__ = {'schema': 'vms4u4'}

    card_bind_status = Column(Numeric(scale=0, asdecimal=False))
    card_bind_user_id = Column(Numeric(scale=0, asdecimal=False))
    card_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    card_no = Column(String(8))
    card_number = Column(String(32))
    card_type = Column(Numeric(scale=0, asdecimal=False))


class TMtCardBusines(Base):
    __tablename__ = 't_mt_card_business'
    __table_args__ = {'schema': 'vms4u4'}

    amount_after = Column(Numeric(scale=0, asdecimal=False))
    amount_before = Column(Numeric(scale=0, asdecimal=False))
    amount_change = Column(Numeric(scale=0, asdecimal=False))
    bind_user_id = Column(Numeric(scale=0, asdecimal=False))
    business_cs_seq = Column(String(32))
    business_desc = Column(String(100))
    business_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    business_time = Column(DateTime)
    business_type = Column(Numeric(scale=0, asdecimal=False))
    business_vs_seq = Column(String(32))
    card_id = Column(Numeric(scale=0, asdecimal=False))
    serial = Column(Numeric(scale=0, asdecimal=False))


class TMtCardEvent(Base):
    __tablename__ = 't_mt_card_event'
    __table_args__ = {'schema': 'vms4u4'}

    event_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    pay_amount = Column(Numeric(scale=0, asdecimal=False))
    total_amount = Column(Numeric(scale=0, asdecimal=False))


class TMtCardRecharge(Base):
    __tablename__ = 't_mt_card_recharge'
    __table_args__ = {'schema': 'vms4u4'}

    bonus_amount = Column(Numeric(scale=0, asdecimal=False))
    card_id = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    pay_amount = Column(Numeric(scale=0, asdecimal=False))
    recharge_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    recharge_seq = Column(String(32))
    recharge_status = Column(Numeric(scale=0, asdecimal=False))
    total_amount = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)


class TMtCardType(Base):
    __tablename__ = 't_mt_card_type'
    __table_args__ = {'schema': 'vms4u4'}

    type_code = Column(Numeric(scale=0, asdecimal=False))
    type_desc = Column(String(30))
    type_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    type_name = Column(String(30))
    type_origin = Column(String(30))


class TMtCompany(Base):
    __tablename__ = 't_mt_company'
    __table_args__ = {'schema': 'vms4u4'}

    company_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    company_level = Column(Numeric(scale=0, asdecimal=False))
    company_name = Column(String(50))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    customer_id = Column(Numeric(scale=0, asdecimal=False))
    p_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    max_vm_capacity = Column(Numeric(9, 0, asdecimal=False))
    delete_mark = Column(Numeric(scale=0, asdecimal=False))
    export_stock = Column(Numeric(scale=0, asdecimal=False))


class TMtCustomer(Base):
    __tablename__ = 't_mt_customer'
    __table_args__ = {'schema': 'vms4u4'}

    collection_account = Column(String(50))
    collection_account_type = Column(String(200))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    customer_address = Column(String(100))
    customer_email = Column(String(50))
    customer_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    customer_mode = Column(Numeric(scale=0, asdecimal=False))
    customer_name = Column(String(50))
    customer_type = Column(Numeric(asdecimal=False))
    manager_name = Column(String(50))
    manager_phone = Column(String(20))
    sync_mark = Column(Numeric(scale=0, asdecimal=False))
    sync_time = Column(DateTime)
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    tax_code = Column(String(50))
    giro_account = Column(String(50))
    giro_account_type = Column(Numeric(scale=0, asdecimal=False))
    giro_account_name = Column(String(50))


class TMtCustomerSecret(Base):
    __tablename__ = 't_mt_customer_secret'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    customer_id = Column(Numeric(scale=0, asdecimal=False))
    customer_public_id = Column(String(20), nullable=False)
    key_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    key_value = Column(String(200))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    _global = Column('global', Numeric(22, 0, asdecimal=False))


class TMtExport(Base):
    __tablename__ = 't_mt_export'
    __table_args__ = {'schema': 'vms4u4'}

    address = Column(String(100))
    company_id = Column(Numeric(scale=0, asdecimal=False))
    create_time = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    export_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    type = Column(Numeric(scale=0, asdecimal=False))
    condition = Column(String(128))


class TMtHardware(Base):
    __tablename__ = 't_mt_hardware'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    hardware_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    hd_category = Column(String(100))
    hd_code = Column(String(30))
    hd_desc = Column(String(500))
    hd_name = Column(String(100))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))


class TMtLine(Base):
    __tablename__ = 't_mt_line'
    __table_args__ = {'schema': 'vms4u4'}

    company_id = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    delete_mark = Column(Numeric(scale=0, asdecimal=False))
    delivery_user_id = Column(Numeric(scale=0, asdecimal=False))
    line_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    line_name = Column(String(100))
    repair_user_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))


class TMtNode(Base):
    __tablename__ = 't_mt_node'
    __table_args__ = {'schema': 'vms4u4'}

    area_id = Column(Numeric(scale=0, asdecimal=False))
    business_district_id = Column(Numeric(scale=0, asdecimal=False))
    company_id = Column(Numeric(scale=0, asdecimal=False))
    delivery_id = Column(Numeric(scale=0, asdecimal=False))
    latitude = Column(String(30))
    fill_type = Column(Numeric(scale=0, asdecimal=False))
    longitude = Column(String(30))
    node_address = Column(String(300))
    node_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    node_name = Column(String(50))
    opt_end_time = Column(String(8))
    opt_start_time = Column(String(8))
    property_level = Column(String(20))
    property_name = Column(String(50))
    property_phone = Column(String(20))
    put_end_time = Column(String(8))
    put_method = Column(String(300))
    put_start_time = Column(String(8))
    repair_id = Column(Numeric(scale=0, asdecimal=False))
    city_id = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    province_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    property_manager = Column(String(50))
    partner_id = Column(Numeric(scale=0, asdecimal=False))
    longitude_mars = Column(String(30))
    latitude_mars = Column(String(30))
    line_id = Column(Numeric(scale=0, asdecimal=False))


class TMtPartner(Base):
    __tablename__ = 't_mt_partner'
    __table_args__ = {'schema': 'vms4u4'}

    collection_account = Column(String(50))
    collection_account_type = Column(String(200))
    company_id = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    manager_name = Column(String(50))
    manager_phone = Column(String(20))
    partner_address = Column(String(200))
    partner_email = Column(String(50))
    partner_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    partner_name = Column(String(100))
    partner_type = Column(Numeric(scale=0, asdecimal=False))
    tax_code = Column(String(50))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    ctor = Column(Numeric(asdecimal=False))


class TMtPayType(Base):
    __tablename__ = 't_mt_pay_type'
    __table_args__ = {'schema': 'vms4u4'}

    code = Column(Numeric(scale=0, asdecimal=False))
    mt_pay_type_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TMtSim(Base):
    __tablename__ = 't_mt_sim'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    delete_mark = Column(Numeric(scale=0, asdecimal=False))
    service_provider = Column(Numeric(scale=0, asdecimal=False))
    sim_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    sim_number = Column(String(20))
    sim_phone = Column(String(20))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TMtSku(Base):
    __tablename__ = 't_mt_sku'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    db_code = Column(String(50))
    img_url = Column(String(250))
    inventory_type = Column(String(50))
    offer_price = Column(Numeric(scale=0, asdecimal=False))
    packing_specification = Column(String(100))
    sku_code = Column(String(50))
    sku_color = Column(String(20))
    sku_desc = Column(String(500))
    sku_height = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    sku_length = Column(Numeric(scale=0, asdecimal=False))
    sku_made_area = Column(String(200))
    sku_name = Column(String(200))
    sku_producer = Column(String(100))
    sku_type = Column(Numeric(scale=0, asdecimal=False))
    sku_unit = Column(String(50))
    sku_width = Column(Numeric(scale=0, asdecimal=False))
    storage_method = Column(String(200))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    sku_shelf_life = Column(Numeric(scale=0, asdecimal=False))
    sale_area = Column(String(200))
    deposit = Column(Numeric(scale=0, asdecimal=False))
    reserve = Column(Numeric(scale=0, asdecimal=False))
    validity_type = Column(Numeric(scale=0, asdecimal=False))
    generalize_type = Column(Numeric(scale=0, asdecimal=False))
    validity_monitor = Column(Numeric(scale=0, asdecimal=False))


class TMtSkuCompany(Base):
    __tablename__ = 't_mt_sku_company'
    __table_args__ = {'schema': 'vms4u4'}

    company_id = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    sku_id = Column(Numeric(scale=0, asdecimal=False))


class TMtSkuType(Base):
    __tablename__ = 't_mt_sku_type'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    p_id = Column(Numeric(scale=0, asdecimal=False), server_default=text("0"))
    type_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    type_name = Column(String(50))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    external_id = Column(String(100))


class TMtTheme(Base):
    __tablename__ = 't_mt_theme'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    theme_desc = Column(String(500))
    theme_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    theme_name = Column(String(50))
    theme_url = Column(String(100))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    preview_urls = Column(String(300))
    delete_mark = Column(Numeric(scale=0, asdecimal=False))
    theme_md5 = Column(String(32))
    theme_size = Column(Numeric(scale=0, asdecimal=False))


class TMtUpgradeDetail(Base):
    __tablename__ = 't_mt_upgrade_detail'
    __table_args__ = {'schema': 'vms4u4'}

    detaild_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    plan_id = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TMtUpgradePackage(Base):
    __tablename__ = 't_mt_upgrade_package'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    disable = Column(Numeric(scale=0, asdecimal=False))
    package_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    package_type = Column(Numeric(scale=0, asdecimal=False))
    package_version = Column(String(50))
    resource_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))


class TMtUpgradePlan(Base):
    __tablename__ = 't_mt_upgrade_plan'
    __table_args__ = {'schema': 'vms4u4'}

    area_id = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    end_time = Column(String(10))
    package_id = Column(Numeric(scale=0, asdecimal=False))
    plan_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    start_time = Column(String(10))
    vm_model_id = Column(Numeric(scale=0, asdecimal=False))


class TMtUpgradeVersion(Base):
    __tablename__ = 't_mt_upgrade_version'
    __table_args__ = {'schema': 'vms4u4'}

    last_downloaded_ver = Column(String(50))
    last_issued_ver = Column(Numeric(scale=0, asdecimal=False))
    last_req_ver = Column(String(50))
    version_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    version_type = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TMtVm(Base):
    __tablename__ = 't_mt_vm'
    __table_args__ = (
        Index('i_t_mt_vm_vn', 'vm_id', 'node_id'),
        {'schema': 'vms4u4'}
    )

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    delete_mark = Column(Numeric(scale=0, asdecimal=False))
    inner_code = Column(String(20))
    node_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_desc = Column(String(200))
    vm_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    vm_model_id = Column(Numeric(scale=0, asdecimal=False))
    vm_status = Column(Numeric(1, 0, asdecimal=False))
    company_id = Column(Numeric(scale=0, asdecimal=False))
    hardware_code = Column(String(100))
    create_task_cycle = Column(Numeric(scale=0, asdecimal=False))
    unique_code = Column(String(32))
    channel_structure = Column(String(500))
    report_sp_count = Column(Numeric(scale=0, asdecimal=False))
    printer = Column(Numeric(scale=0, asdecimal=False))
    reserve = Column(Numeric(scale=0, asdecimal=False))
    stock_status = Column(Numeric(scale=0, asdecimal=False))


class TMtVmChannel(Base):
    __tablename__ = 't_mt_vm_channel'
    __table_args__ = {'schema': 'vms4u4'}

    channel_code = Column(String(10))
    channel_height = Column(Numeric(scale=0, asdecimal=False))
    channel_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    channel_long = Column(Numeric(scale=0, asdecimal=False))
    channel_status = Column(Numeric(scale=0, asdecimal=False))
    channel_width = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    curr_num = Column(Numeric(scale=0, asdecimal=False))
    inner_code = Column(String(20))
    max_capacity = Column(Numeric(scale=0, asdecimal=False))
    merge_qty = Column(Numeric(scale=0, asdecimal=False))
    pc_type = Column(Numeric(scale=0, asdecimal=False))
    remarks = Column(String(200))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False), index=True)
    accept_width_max = Column(Numeric(9, 0, asdecimal=False))
    display_mode = Column(Numeric(9, 0, asdecimal=False))
    fixed_capacity = Column(Numeric(9, 0, asdecimal=False))
    is_fixed_capacity = Column(Numeric(9, 0, asdecimal=False))
    merge_pid = Column(Numeric(9, 0, asdecimal=False))
    has_next_stock = Column(Numeric(scale=0, asdecimal=False))
    manufacture_end_date = Column(DateTime)
    manufacture_date = Column(DateTime)
    batch_no = Column(String(100))


class TMtVmChannelNext(Base):
    __tablename__ = 't_mt_vm_channel_next'
    __table_args__ = {'schema': 'vms4u4'}

    channel_code = Column(String(10))
    channel_height = Column(Numeric(scale=0, asdecimal=False))
    channel_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    channel_long = Column(Numeric(scale=0, asdecimal=False))
    channel_status = Column(Numeric(scale=0, asdecimal=False))
    channel_width = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    inner_code = Column(String(20))
    max_capacity = Column(Numeric(scale=0, asdecimal=False))
    merge_qty = Column(Numeric(scale=0, asdecimal=False))
    pc_type = Column(Numeric(scale=0, asdecimal=False))
    remarks = Column(String(200))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    accept_width_max = Column(Numeric(9, 0, asdecimal=False))
    display_mode = Column(Numeric(9, 0, asdecimal=False))
    fixed_capacity = Column(Numeric(9, 0, asdecimal=False))
    is_fixed_capacity = Column(Numeric(9, 0, asdecimal=False))
    merge_pid = Column(Numeric(9, 0, asdecimal=False))


class TMtVmEnergySave(Base):
    __tablename__ = 't_mt_vm_energy_save'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    end_time = Column(String(8))
    energy_save_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    energy_save_type = Column(Numeric(scale=0, asdecimal=False))
    remark = Column(String(200))
    start_time = Column(String(8))
    target_value = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    temp_box_no = Column(Numeric(scale=0, asdecimal=False))


class TMtVmEvent(Base):
    __tablename__ = 't_mt_vm_event'
    __table_args__ = {'schema': 'vms4u4'}

    discount_type = Column(Numeric(scale=0, asdecimal=False))
    discount_value = Column(Numeric(scale=0, asdecimal=False))
    event_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    event_type = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    master_id = Column(Numeric(scale=0, asdecimal=False))


class TMtVmEventMaster(Base):
    __tablename__ = 't_mt_vm_event_master'
    __table_args__ = {'schema': 'vms4u4'}

    begin_date = Column(DateTime)
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    end_date = Column(DateTime)
    end_time = Column(String(8))
    event_desc = Column(String(200))
    event_name = Column(String(100))
    event_type = Column(Numeric(scale=0, asdecimal=False))
    master_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    start_time = Column(String(8))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    event_id = Column(Numeric(scale=0, asdecimal=False))


class TMtVmLock(Base):
    __tablename__ = 't_mt_vm_lock'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    effect_date = Column(DateTime)
    key_status = Column(Numeric(scale=0, asdecimal=False))
    lock_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    lock_key = Column(String(20))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False), unique=True)


class TMtVmMaintenance(Base):
    __tablename__ = 't_mt_vm_maintenance'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    maintenance_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    operation_desc = Column(String(200))
    operation_type = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    uvp_uid = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TMtVmModel(Base):
    __tablename__ = 't_mt_vm_model'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    model_desc = Column(String(200))
    model_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    model_name = Column(String(50))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    allow_merge = Column(Numeric(scale=0, asdecimal=False))
    channel_layer_qty = Column(Numeric(scale=0, asdecimal=False))
    channel_per_layer = Column(String(100))
    channel_type = Column(Numeric(scale=0, asdecimal=False))
    coling_hdid = Column(Numeric(scale=0, asdecimal=False))
    company_init = Column(String(10))
    cooling_yn = Column(Numeric(scale=0, asdecimal=False))
    elevator_platform_hdid = Column(Numeric(scale=0, asdecimal=False))
    elevator_platform_yn = Column(Numeric(scale=0, asdecimal=False))
    hitting_hdid = Column(Numeric(scale=0, asdecimal=False))
    machine_view = Column(Numeric(scale=0, asdecimal=False))
    rfid_card = Column(Numeric(scale=0, asdecimal=False))
    screen_size = Column(Numeric(scale=0, asdecimal=False))
    screen_type = Column(Numeric(scale=0, asdecimal=False))
    shortcuts_qty = Column(Numeric(scale=0, asdecimal=False))
    temperature_handler_mode = Column(Numeric(scale=0, asdecimal=False))
    vend_out_architecture = Column(Numeric(scale=0, asdecimal=False))
    vend_out_type = Column(Numeric(scale=0, asdecimal=False))
    hitting_yn = Column(Numeric(asdecimal=False))
    temp_box_count = Column(Numeric(scale=0, asdecimal=False))
    temp_ctrl_desc = Column(String(200))
    delete_mark = Column(Numeric(scale=0, asdecimal=False))


class TMtVmModelChannel(Base):
    __tablename__ = 't_mt_vm_model_channel'
    __table_args__ = {'schema': 'vms4u4'}

    accept_width_max = Column(Numeric(scale=0, asdecimal=False))
    channel_code = Column(String(10))
    channel_height = Column(Numeric(scale=0, asdecimal=False))
    channel_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    channel_length = Column(Numeric(scale=0, asdecimal=False))
    channel_type = Column(Numeric(scale=0, asdecimal=False))
    channel_width = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    model_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    display_mode = Column(Numeric(scale=0, asdecimal=False))
    fixed_capacity = Column(Numeric(scale=0, asdecimal=False))
    is_fixed_capacity = Column(Numeric(scale=0, asdecimal=False))


class TMtVmPrice(Base):
    __tablename__ = 't_mt_vm_price'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    price_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    real_price = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TMtVmResource(Base):
    __tablename__ = 't_mt_vm_resource'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    delete_mark = Column(Numeric(scale=0, asdecimal=False))
    preview_urls = Column(String(300))
    resource_desc = Column(String(500))
    resource_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    resource_md5 = Column(String(32))
    resource_name = Column(String(50))
    resource_size = Column(Numeric(scale=0, asdecimal=False))
    resource_url = Column(String(100))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    resource_type = Column(Numeric(scale=0, asdecimal=False))


class TMtVmShortcut(Base):
    __tablename__ = 't_mt_vm_shortcuts'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    shortcuts_code = Column(String(5))
    shortcuts_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    bind_id = Column(Numeric(scale=0, asdecimal=False))
    bind_type = Column(Numeric(scale=0, asdecimal=False))


class TMtVmSkuSort(Base):
    __tablename__ = 't_mt_vm_sku_sort'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    sort = Column(Numeric(scale=0, asdecimal=False))
    sort_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


t_t_mt_vm_sku_sort_1009 = Table(
    't_mt_vm_sku_sort_1009', metadata,
    Column('ctime', DateTime),
    Column('ctor', Numeric(scale=0, asdecimal=False)),
    Column('sku_id', Numeric(scale=0, asdecimal=False)),
    Column('sort', Numeric(scale=0, asdecimal=False)),
    Column('sort_id', Numeric(scale=0, asdecimal=False), nullable=False),
    Column('utime', DateTime),
    Column('utor', Numeric(scale=0, asdecimal=False)),
    Column('vm_id', Numeric(scale=0, asdecimal=False)),
    schema='vms4u4'
)


class TMtVmSkuSortNext(Base):
    __tablename__ = 't_mt_vm_sku_sort_next'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    sort = Column(Numeric(scale=0, asdecimal=False))
    sort_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TMtVmStrategy(Base):
    __tablename__ = 't_mt_vm_strategy'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    strategy_name = Column(String(100))
    sysn_mark = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    strategy_type = Column(Numeric(scale=0, asdecimal=False))
    strategy_id = Column(Numeric(asdecimal=False), primary_key=True)


class TMtVmText(Base):
    __tablename__ = 't_mt_vm_text'
    __table_args__ = {'schema': 'vms4u4'}

    begin_date = Column(DateTime)
    customer_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    end_date = Column(DateTime)
    text_contents = Column(String(2000))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TMtVmTheme(Base):
    __tablename__ = 't_mt_vm_theme'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    theme_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TMtVmThemeFormat(Base):
    __tablename__ = 't_mt_vm_theme_format'
    __table_args__ = {'schema': 'vms4u4'}

    id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    theme_format = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TMtVmVersion(Base):
    __tablename__ = 't_mt_vm_version'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    device_name = Column(String(50))
    device_version = Column(String(50))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    version_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    device_type = Column(Numeric(scale=0, asdecimal=False))


class TMtWarehouse(Base):
    __tablename__ = 't_mt_warehouse'
    __table_args__ = {'schema': 'vms4u4'}

    contact_name = Column(String(20))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    dimensions = Column(String(20))
    longitude = Column(String(20))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    warehouse_address = Column(String(100))
    warehouse_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    warehouse_level = Column(Numeric(scale=0, asdecimal=False))
    warehouse_manager = Column(String(50))
    warehouse_name = Column(String(50))
    warehouse_phone = Column(String(20))
    warehouse_type = Column(String(1))
    warehouse_code = Column(String(9))


class TMtWarehouseArea(Base):
    __tablename__ = 't_mt_warehouse_area'
    __table_args__ = {'schema': 'vms4u4'}

    area_id = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    warehouse_id = Column(Numeric(scale=0, asdecimal=False))
    wa_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)


class TOpenVmSecret(Base):
    __tablename__ = 't_open_vm_secret'
    __table_args__ = {'schema': 'vms4u4'}

    id = Column(Numeric(22, 0, asdecimal=False), primary_key=True)
    secret_id = Column(Numeric(22, 0, asdecimal=False))
    vm_id = Column(Numeric(22, 0, asdecimal=False))


class TOrderCost(Base):
    __tablename__ = 't_order_cost'
    __table_args__ = {'schema': 'vms4u4'}

    cost_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    order_id = Column(Numeric(scale=0, asdecimal=False), unique=True)
    pay_price = Column(Numeric(scale=0, asdecimal=False))
    pay_status = Column(Numeric(scale=0, asdecimal=False))
    pay_time = Column(DateTime)
    pay_type = Column(Numeric(scale=0, asdecimal=False))


class TOrderCostDetail(Base):
    __tablename__ = 't_order_cost_detail'
    __table_args__ = {'schema': 'vms4u4'}

    cost_account = Column(String(100))
    cost_detail_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    cost_id = Column(Numeric(scale=0, asdecimal=False), index=True)
    cost_price = Column(Numeric(scale=0, asdecimal=False))
    cost_type = Column(Numeric(scale=0, asdecimal=False))
    pay_outer_seq = Column(String(100))


class TOrderDetail(Base):
    __tablename__ = 't_order_detail'
    __table_args__ = (
        Index('i_t_order_detail_vtv', 'valid_time', 'vendout_status'),
        Index('i_t_order_detail_vtr', 'valid_time', 'refund_status'),
        Index('i_t_order_detail_vtos', 'valid_time', 'order_id', 'sku_id'),
        {'schema': 'vms4u4'}
    )

    deposit_id = Column(Numeric(scale=0, asdecimal=False))
    deposit_refund_status = Column(Numeric(scale=0, asdecimal=False))
    discount_price = Column(Numeric(scale=0, asdecimal=False))
    extract_code = Column(String(10))
    memo = Column(String(200))
    notify_status = Column(Numeric(scale=0, asdecimal=False))
    order_detail_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    order_id = Column(Numeric(scale=0, asdecimal=False), index=True)
    real_price = Column(Numeric(scale=0, asdecimal=False))
    refund_status = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    sku_price = Column(Numeric(scale=0, asdecimal=False))
    status = Column(Numeric(scale=0, asdecimal=False))
    valid_time = Column(String(20))
    vendout_status = Column(Numeric(scale=0, asdecimal=False))


class TOrderDetailCallback(Base):
    __tablename__ = 't_order_detail_callback'
    __table_args__ = {'schema': 'vms4u4'}

    callback_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    callback_status = Column(Numeric(scale=0, asdecimal=False))
    callback_time = Column(DateTime)
    order_detail_id = Column(Numeric(scale=0, asdecimal=False), unique=True)


class TOrderDetailCost(Base):
    __tablename__ = 't_order_detail_cost'
    __table_args__ = {'schema': 'vms4u4'}

    cost_account = Column(String(100))
    cost_price = Column(Numeric(scale=0, asdecimal=False))
    cost_type = Column(Numeric(scale=0, asdecimal=False))
    order_detail_cost_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    order_detail_id = Column(Numeric(scale=0, asdecimal=False), index=True)


class TOrderDetailDeposit(Base):
    __tablename__ = 't_order_detail_deposit'
    __table_args__ = {'schema': 'vms4u4'}

    deposit_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    deposit_price = Column(Numeric(scale=0, asdecimal=False))
    order_detail_id = Column(Numeric(scale=0, asdecimal=False))
    refund_id = Column(Numeric(scale=0, asdecimal=False))


class TOrderDetailNotify(Base):
    __tablename__ = 't_order_detail_notify'
    __table_args__ = {'schema': 'vms4u4'}

    customer_public_id = Column(String(50))
    notify_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    notify_status = Column(Numeric(scale=0, asdecimal=False))
    notify_time = Column(DateTime)
    notify_url = Column(String(200))
    order_detail_id = Column(Numeric(scale=0, asdecimal=False), unique=True)


class TOrderDetailVendout(Base):
    __tablename__ = 't_order_detail_vendout'
    __table_args__ = {'schema': 'vms4u4'}

    order_detail_id = Column(Numeric(scale=0, asdecimal=False), unique=True)
    vendout_channel_id = Column(Numeric(scale=0, asdecimal=False))
    vendout_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    vendout_status = Column(Numeric(scale=0, asdecimal=False))
    vendout_time = Column(DateTime)
    u_time = Column(DateTime)


class TOrderDiscount(Base):
    __tablename__ = 't_order_discount'
    __table_args__ = {'schema': 'vms4u4'}

    coupon_id = Column(Numeric(scale=0, asdecimal=False))
    discount_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    order_id = Column(Numeric(scale=0, asdecimal=False))


class TOrderMaster(Base):
    __tablename__ = 't_order_master'
    __table_args__ = {'schema': 'vms4u4'}

    company_id = Column(Numeric(scale=0, asdecimal=False))
    consignee_id = Column(Numeric(scale=0, asdecimal=False))
    cost_status = Column(Numeric(scale=0, asdecimal=False))
    c_time = Column(DateTime)
    discount_total_price = Column(Numeric(scale=0, asdecimal=False))
    order_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    order_seq = Column(String(50), nullable=False, unique=True)
    order_type = Column(String(20))
    pay_id = Column(Numeric(scale=0, asdecimal=False))
    real_total_price = Column(Numeric(scale=0, asdecimal=False), nullable=False)
    total_price = Column(Numeric(scale=0, asdecimal=False), nullable=False)
    vm_id = Column(Numeric(scale=0, asdecimal=False))


class TOrderRefund(Base):
    __tablename__ = 't_order_refund'
    __table_args__ = {'schema': 'vms4u4'}

    account = Column(String(50))
    amount = Column(Numeric(scale=0, asdecimal=False))
    ctor = Column(Numeric(scale=0, asdecimal=False))
    order_detail_id = Column(Numeric(scale=0, asdecimal=False))
    order_id = Column(Numeric(scale=0, asdecimal=False))
    refund_desc = Column(String(200))
    refund_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    refund_seq = Column(String(50))
    refund_status = Column(Numeric(scale=0, asdecimal=False))
    refund_time = Column(DateTime)
    spare = Column(String(50))
    type = Column(Numeric(scale=0, asdecimal=False))


class TPayMaster(Base):
    __tablename__ = 't_pay_master'
    __table_args__ = {'schema': 'vms4u4'}

    account = Column(String(50))
    amount = Column(Numeric(scale=0, asdecimal=False))
    create_time = Column(DateTime)
    effective_time = Column(DateTime)
    order_seq = Column(String(32))
    origin = Column(String(20))
    other_parameter = Column(String(50))
    other_seq = Column(String(50))
    payment_amount = Column(Numeric(scale=0, asdecimal=False))
    payment_time = Column(DateTime)
    pay_desc = Column(String(100))
    pay_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    pay_result = Column(Numeric(scale=0, asdecimal=False))
    pay_status = Column(Numeric(scale=0, asdecimal=False))
    pay_title = Column(String(200))
    pay_type = Column(Numeric(scale=0, asdecimal=False))
    url = Column(String(100))


class TPayRefund(Base):
    __tablename__ = 't_pay_refund'
    __table_args__ = {'schema': 'vms4u4'}

    pay_id = Column(Numeric(scale=0, asdecimal=False))
    refund_amount = Column(Numeric(scale=0, asdecimal=False))
    refund_create_time = Column(DateTime)
    refund_desc = Column(String(100))
    refund_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    refund_seq = Column(String(50))
    refund_status = Column(Numeric(scale=0, asdecimal=False))


class TProOrder(Base):
    __tablename__ = 't_pro_order'
    __table_args__ = {'schema': 'vms4u4'}

    cost_amount = Column(Numeric(scale=0, asdecimal=False))
    cost_status = Column(Numeric(scale=0, asdecimal=False))
    cost_time = Column(DateTime)
    cost_type = Column(Numeric(scale=0, asdecimal=False))
    create_time = Column(DateTime)
    deal_seq = Column(String(50))
    order_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    seq = Column(String(50), unique=True)
    total_amount = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    wechat_package = Column(String(50))
    wechat_user_id = Column(Numeric(scale=0, asdecimal=False))


class TProOrderGood(Base):
    __tablename__ = 't_pro_order_good'
    __table_args__ = {'schema': 'vms4u4'}

    base_price = Column(Numeric(scale=0, asdecimal=False))
    channel_id = Column(Numeric(scale=0, asdecimal=False))
    extract_code = Column(String(8))
    good_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    notify_status = Column(Numeric(scale=0, asdecimal=False))
    notify_time = Column(DateTime)
    order_id = Column(Numeric(scale=0, asdecimal=False))
    real_price = Column(Numeric(scale=0, asdecimal=False))
    refund_seq = Column(String(50))
    refund_status = Column(Numeric(scale=0, asdecimal=False))
    refund_time = Column(DateTime)
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    valid_time = Column(String(12))
    vendout_status = Column(Numeric(scale=0, asdecimal=False))
    vendout_time = Column(DateTime)


class TReserveStatistic(Base):
    __tablename__ = 't_reserve_statistics'
    __table_args__ = {'schema': 'vms4u4'}

    distribution_num = Column(Numeric(scale=0, asdecimal=False))
    pending_num = Column(Numeric(scale=0, asdecimal=False))
    reserve_statistics_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    statistics_date = Column(String(10))
    totalnum = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))


t_t_sale_seq_factor = Table(
    't_sale_seq_factor', metadata,
    Column('ctime', DateTime),
    Column('host_name', String(20)),
    Column('sale_seq_factor_id', Numeric(scale=0, asdecimal=False)),
    schema='vms4u4'
)


class TSlOrderCallback(Base):
    __tablename__ = 't_sl_order_callback'
    __table_args__ = {'schema': 'vms4u4'}

    callback_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    callback_status = Column(Numeric(scale=0, asdecimal=False))
    callback_time = Column(DateTime)
    order_id = Column(Numeric(scale=0, asdecimal=False), unique=True)


class TSlOrderCost(Base):
    __tablename__ = 't_sl_order_cost'
    __table_args__ = (
        Index('i_t_sl_order_cost_occ', 'order_id', 'cost_time', 'cost_status'),
        {'schema': 'vms4u4'}
    )

    cost_account = Column(String(50))
    cost_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    cost_price = Column(Numeric(scale=0, asdecimal=False))
    cost_status = Column(Numeric(scale=0, asdecimal=False))
    cost_time = Column(DateTime)
    cost_type = Column(Numeric(scale=0, asdecimal=False))
    deal_seq = Column(String(50))
    order_id = Column(Numeric(scale=0, asdecimal=False), unique=True)


class TSlOrderGiro(Base):
    __tablename__ = 't_sl_order_giro'
    __table_args__ = {'schema': 'vms4u4'}

    account_name = Column(String(20))
    account_no = Column(String(50))
    account_type = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    giro_month = Column(String(7))
    giro_seq = Column(String(50))
    giro_status = Column(Numeric(scale=0, asdecimal=False))
    giro_time = Column(DateTime)
    order_giro_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    s_user_id = Column(Numeric(scale=0, asdecimal=False))
    transfer_amount = Column(Numeric(scale=0, asdecimal=False))


class TSlOrderMaster(Base):
    __tablename__ = 't_sl_order_master'
    __table_args__ = {'schema': 'vms4u4'}

    company_id = Column(Numeric(scale=0, asdecimal=False))
    order_desc = Column(String(200))
    order_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    order_price = Column(Numeric(scale=0, asdecimal=False))
    order_seq = Column(String(50), nullable=False)
    order_time = Column(DateTime)
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    spare = Column(String(50))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    vendout_status = Column(Numeric(scale=0, asdecimal=False))


class TSlOrderNotify(Base):
    __tablename__ = 't_sl_order_notify'
    __table_args__ = {'schema': 'vms4u4'}

    customer_public_id = Column(String(20))
    notify_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    notify_status = Column(Numeric(scale=0, asdecimal=False))
    notify_time = Column(DateTime)
    notify_url = Column(String(200))
    order_id = Column(Numeric(scale=0, asdecimal=False), unique=True)


class TSlOrderRefund(Base):
    __tablename__ = 't_sl_order_refund'
    __table_args__ = {'schema': 'vms4u4'}

    account = Column(String(50))
    amount = Column(Numeric(scale=0, asdecimal=False))
    ctor = Column(Numeric(scale=0, asdecimal=False))
    refund_desc = Column(String(200))
    refund_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    refund_seq = Column(String(50), nullable=False)
    refund_status = Column(Numeric(scale=0, asdecimal=False))
    refund_time = Column(DateTime)
    spare = Column(String(50))
    type = Column(Numeric(scale=0, asdecimal=False))
    order_id = Column(Numeric(scale=0, asdecimal=False), unique=True)


class TSlOrderSale(Base):
    __tablename__ = 't_sl_order_sale'
    __table_args__ = {'schema': 'vms4u4'}

    sale_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    sale_seq = Column(String(50), nullable=False, unique=True)
    sale_desc = Column(String(200))
    sale_status = Column(Numeric(scale=0, asdecimal=False))
    sale_time = Column(DateTime)
    vm_id = Column(Numeric(scale=0, asdecimal=False), index=True)
    vm_channel_id = Column(Numeric(scale=0, asdecimal=False), index=True)
    sku_id = Column(Numeric(scale=0, asdecimal=False), index=True)
    sku_name = Column(String(50))
    cost_time = Column(DateTime)
    cost_status = Column(Numeric(scale=0, asdecimal=False))
    out_status = Column(Numeric(scale=0, asdecimal=False))
    out_time = Column(DateTime)
    pay_account = Column(String(50))
    pay_amount = Column(Numeric(scale=0, asdecimal=False))
    pay_type = Column(Numeric(scale=0, asdecimal=False))
    real_price = Column(Numeric(scale=0, asdecimal=False))
    company_id = Column(Numeric(scale=0, asdecimal=False))
    ctor = Column(Numeric(scale=0, asdecimal=False))
    deal_seq = Column(String(50))
    notify_status = Column(Numeric(scale=0, asdecimal=False))
    notify_time = Column(DateTime)
    sync_mark = Column(Numeric(asdecimal=False))
    sync_time = Column(DateTime)
    spare = Column(String(50))


class TSlOrderVendout(Base):
    __tablename__ = 't_sl_order_vendout'
    __table_args__ = {'schema': 'vms4u4'}

    order_id = Column(Numeric(scale=0, asdecimal=False), nullable=False, unique=True)
    vendout_channel_id = Column(Numeric(scale=0, asdecimal=False))
    vendout_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    vendout_status = Column(Numeric(scale=0, asdecimal=False))
    vendout_time = Column(DateTime)


class TStVmChannel(Base):
    __tablename__ = 't_st_vm_channel'
    __table_args__ = {'schema': 'vms4u4'}

    channel_code = Column(String(10))
    channel_height = Column(Numeric(scale=0, asdecimal=False))
    channel_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    channel_long = Column(Numeric(scale=0, asdecimal=False))
    channel_width = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    inner_code = Column(String(20))
    max_capacity = Column(Numeric(scale=0, asdecimal=False))
    merge_qty = Column(Numeric(scale=0, asdecimal=False))
    pc_type = Column(Numeric(scale=0, asdecimal=False))
    remarks = Column(String(200))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    strategy_id = Column(Numeric(scale=0, asdecimal=False))
    accept_width_max = Column(Numeric(9, 0, asdecimal=False))
    display_mode = Column(Numeric(9, 0, asdecimal=False))
    fixed_capacity = Column(Numeric(9, 0, asdecimal=False))
    is_fixed_capacity = Column(Numeric(9, 0, asdecimal=False))
    merge_pid = Column(Numeric(9, 0, asdecimal=False))


class TStVmEnergySave(Base):
    __tablename__ = 't_st_vm_energy_save'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    customer_id = Column(Numeric(scale=0, asdecimal=False))
    end_time = Column(String(8))
    energy_save_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    energy_save_type = Column(Numeric(scale=0, asdecimal=False))
    remark = Column(String(200))
    start_time = Column(String(8))
    target_value = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    strategy_id = Column(Numeric(scale=0, asdecimal=False))
    temp_box_no = Column(Numeric(scale=0, asdecimal=False))


class TStVmEvent(Base):
    __tablename__ = 't_st_vm_event'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    discount_type = Column(Numeric(scale=0, asdecimal=False))
    discount_value = Column(Numeric(scale=0, asdecimal=False))
    event_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    event_type = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    strategy_id = Column(Numeric(scale=0, asdecimal=False))


class TStVmPrice(Base):
    __tablename__ = 't_st_vm_price'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    price_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    real_price = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    strategy_id = Column(Numeric(scale=0, asdecimal=False))


class TStVmShortcut(Base):
    __tablename__ = 't_st_vm_shortcuts'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    shortcuts_code = Column(String(5))
    shortcuts_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    strategy_id = Column(Numeric(scale=0, asdecimal=False))
    bind_id = Column(Numeric(scale=0, asdecimal=False))
    bind_type = Column(Numeric(scale=0, asdecimal=False))


class TStVmSkuSort(Base):
    __tablename__ = 't_st_vm_sku_sort'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    sort = Column(Numeric(scale=0, asdecimal=False))
    sort_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    strategy_id = Column(Numeric(scale=0, asdecimal=False))


class TStVmStrategy(Base):
    __tablename__ = 't_st_vm_strategy'
    __table_args__ = {'schema': 'vms4u4'}

    company_id = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    strategy_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    strategy_name = Column(String(100))
    strategy_type = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    strategy_desc = Column(String(200))
    model_id = Column(Numeric(9, 0, asdecimal=False))


class TStVmText(Base):
    __tablename__ = 't_st_vm_text'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    end_time = Column(String(8))
    start_time = Column(String(8))
    st_text_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    text_contents = Column(String(2000))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    strategy_id = Column(Numeric(scale=0, asdecimal=False))


class TSysDictionary(Base):
    __tablename__ = 't_sys_dictionary'
    __table_args__ = {'schema': 'vms4u4'}

    dictionary_code = Column(String(50))
    dictionary_desc = Column(String(200))
    dictionary_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    dictionary_name = Column(String(50))
    dictionary_state = Column(Numeric(scale=0, asdecimal=False))
    type_info_id = Column(Numeric(scale=0, asdecimal=False))


class TSysElement(Base):
    __tablename__ = 't_sys_element'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    element_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    element_type = Column(String(50))
    element_url = Column(String(100))
    p_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    element_code = Column(String(50))
    element_index = Column(Numeric(asdecimal=False))
    element_name = Column(String(50))
    element_module = Column(Numeric(scale=0, asdecimal=False))


t_t_sys_element_bak = Table(
    't_sys_element_bak', metadata,
    Column('ctime', DateTime),
    Column('ctor', Numeric(scale=0, asdecimal=False)),
    Column('element_code', String(50)),
    Column('element_id', Numeric(scale=0, asdecimal=False), nullable=False),
    Column('element_index', Numeric(scale=0, asdecimal=False)),
    Column('element_module', Numeric(scale=0, asdecimal=False)),
    Column('element_name', String(50)),
    Column('element_type', String(50)),
    Column('element_url', String(100)),
    Column('p_id', Numeric(scale=0, asdecimal=False)),
    Column('utime', DateTime),
    Column('utor', Numeric(scale=0, asdecimal=False)),
    schema='vms4u4'
)


class TSysLog(Base):
    __tablename__ = 't_sys_log'
    __table_args__ = {'schema': 'vms4u4'}

    log_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    log_operation = Column(String(500))
    log_time = Column(DateTime)
    module_id = Column(Numeric(scale=0, asdecimal=False))
    user_id = Column(Numeric(scale=0, asdecimal=False))
    log_desc = Column(String(2000))


class TSysRole(Base):
    __tablename__ = 't_sys_role'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    company_id = Column(Numeric(scale=0, asdecimal=False))
    role_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    role_level = Column(Numeric(scale=0, asdecimal=False))
    role_name = Column(String(50))
    role_type = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    role_desc = Column(String(200))


class TSysRoleElement(Base):
    __tablename__ = 't_sys_role_element'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    element_id = Column(Numeric(scale=0, asdecimal=False))
    re_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    role_id = Column(Numeric(scale=0, asdecimal=False))


class TSysSetting(Base):
    __tablename__ = 't_sys_settings'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    key = Column(String(32))
    name = Column(String(256))
    pid = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    value = Column(String(256))


class TSysTypeInfo(Base):
    __tablename__ = 't_sys_type_info'
    __table_args__ = {'schema': 'vms4u4'}

    type_code = Column(String(50))
    type_desc = Column(String(200))
    type_info_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    type_name = Column(String(50))


class TSysUser(Base):
    __tablename__ = 't_sys_user'
    __table_args__ = {'schema': 'vms4u4'}

    company_id = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    delete_mark = Column(Numeric(scale=0, asdecimal=False))
    fail_count = Column(Numeric(scale=0, asdecimal=False))
    lock_mark = Column(Numeric(scale=0, asdecimal=False), server_default=text("0"))
    login_time = Column(DateTime, server_default=text("sysdate"))
    user_account = Column(String(32))
    user_email = Column(String(50))
    user_gender = Column(Numeric(scale=0, asdecimal=False))
    user_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    user_name = Column(String(20))
    user_password = Column(String(32))
    user_phone = Column(String(20))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))


class TSysUserBind(Base):
    __tablename__ = 't_sys_user_bind'
    __table_args__ = {'schema': 'vms4u4'}

    bind_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    bind_type = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    outer_id = Column(String(50))
    user_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))


class TSysUserRole(Base):
    __tablename__ = 't_sys_user_role'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    role_id = Column(Numeric(scale=0, asdecimal=False))
    ur_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    user_id = Column(Numeric(scale=0, asdecimal=False))


class TTaskPickupHi(Base):
    __tablename__ = 't_task_pickup_his'
    __table_args__ = {'schema': 'vms4u4'}

    pickup_count = Column(Numeric(scale=0, asdecimal=False))
    pickup_sku_id = Column(Numeric(scale=0, asdecimal=False))
    pickup_time = Column(DateTime)
    pickup_user_id = Column(Numeric(scale=0, asdecimal=False))
    task_pickup_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)


class TTaskVm(Base):
    __tablename__ = 't_task_vm'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    finish_time = Column(DateTime)
    owner = Column(Numeric(scale=0, asdecimal=False))
    task_level = Column(Numeric(scale=0, asdecimal=False))
    task_remarks = Column(String(2000))
    task_time = Column(DateTime)
    task_title = Column(String(200))
    task_type = Column(Numeric(scale=0, asdecimal=False))
    task_vm_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    task_vm_status = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    company_id = Column(Numeric(asdecimal=False))
    already_pickup = Column(Numeric(scale=0, asdecimal=False))
    delivery_pattern = Column(Numeric(scale=0, asdecimal=False))


class TTaskVmChannel(Base):
    __tablename__ = 't_task_vm_channel'
    __table_args__ = {'schema': 'vms4u4'}

    channel_id = Column(Numeric(scale=0, asdecimal=False))
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    sku_count = Column(Numeric(scale=0, asdecimal=False))
    sku_id = Column(Numeric(scale=0, asdecimal=False))
    task_type = Column(Numeric(scale=0, asdecimal=False))
    task_vm_channel_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    task_vm_id = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    task_remarks = Column(String(200))
    after_delivery_stock = Column(Numeric(scale=0, asdecimal=False))
    before_delivery_stock = Column(Numeric(scale=0, asdecimal=False))
    real_count = Column(Numeric(scale=0, asdecimal=False))
    real_clear = Column(Numeric(scale=0, asdecimal=False))
    max_count = Column(Numeric(scale=0, asdecimal=False))
    clear_count = Column(Numeric(scale=0, asdecimal=False))


class TUserCompany(Base):
    __tablename__ = 't_user_company'
    __table_args__ = {'schema': 'vms4u4'}

    company_id = Column(Numeric(scale=0, asdecimal=False))
    user_company_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    user_id = Column(Numeric(scale=0, asdecimal=False))


class TVmOperationHi(Base):
    __tablename__ = 't_vm_operation_his'
    __table_args__ = {'schema': 'vms4u4'}

    client_time = Column(DateTime)
    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    vm_door_status = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False), index=True)
    vm_operation_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)


class TVmStatu(Base):
    __tablename__ = 't_vm_status'
    __table_args__ = {'schema': 'vms4u4'}

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    utime = Column(DateTime)
    utor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    vm_status_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    temp_value = Column(String(500))
    vm_status = Column(Numeric(scale=0, asdecimal=False))
    vm_status_datail = Column(String(2000))
    vm_time = Column(DateTime)
    vm_door_status = Column(Numeric(scale=0, asdecimal=False))


class TVmStatusHi(Base):
    __tablename__ = 't_vm_status_his'
    __table_args__ = (
        Index('i_t_vm_status_his_vvt', 'vm_id', 'vm_time'),
        {'schema': 'vms4u4'}
    )

    ctime = Column(DateTime)
    ctor = Column(Numeric(scale=0, asdecimal=False))
    vm_id = Column(Numeric(scale=0, asdecimal=False))
    vm_status_his_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    temp_value = Column(String(500))
    vm_status = Column(Numeric(scale=0, asdecimal=False))
    vm_status_datail = Column(String(2000))
    vm_time = Column(DateTime)
    door_status = Column(Numeric(scale=0, asdecimal=False))


class TWechatUser(Base):
    __tablename__ = 't_wechat_user'
    __table_args__ = {'schema': 'vms4u4'}

    nick_name = Column(String(32))
    open_id = Column(String(50), nullable=False, unique=True)
    phone = Column(String(15))
    photo = Column(String(500))
    user_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)


class TWechatUserHistory(Base):
    __tablename__ = 't_wechat_user_history'
    __table_args__ = {'schema': 'vms4u4'}

    history_id = Column(Numeric(scale=0, asdecimal=False), primary_key=True)
    user_id = Column(Numeric(scale=0, asdecimal=False))
    use_date = Column(DateTime)
    vm_id = Column(Numeric(scale=0, asdecimal=False))
