#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Author: wufeng


import vms4_models_pro as models
import os, datetime
import inspect
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, aliased
from openpyxl import Workbook

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header


def sendmail(to_user, send_date, file_name=''):
    # 第三方 SMTP 服务
    mail_host = "smtp.ym.163.com"  # 设置服务器
    mail_user = "wufeng@wufeng.space"  # 用户名
    mail_pass = "shamao$2016"  # 口令
    sender = 'wufeng@wufeng.space'
    receivers = to_user  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

    # 创建一个带附件的实例
    message = MIMEMultipart()
    message['From'] = Header("wufeng", 'utf-8')
    message['To'] = Header("王蜜蜜", 'utf-8')
    subject = str(send_date) + '日导出预定订单交易流水'
    message['Subject'] = Header(subject, 'utf-8')

    # 邮件正文内容
    message.attach(MIMEText(str(send_date) + '日导出预定订单交易流水 \n 请查收 ！～', 'plain', 'utf-8'))

    # 构造附件1，传送当前目录下的 test.txt 文件
    if file_name != '':
        att1 = MIMEText(open(file_name, 'rb').read(), 'base64', 'utf-8')
        att1["Content-Type"] = 'application/octet-stream'
        # 这里的filename可以任意写，写什么名字，邮件中显示什么名字
        att1["Content-Disposition"] = 'attachment; filename= %s.xlsx' % (str(send_date))
        message.attach(att1)
    #try:
    smtpObj = smtplib.SMTP_SSL()
    smtpObj.connect(mail_host, 994)  # 25 为 SMTP 端口号
    smtpObj.login(mail_user, mail_pass)
    smtpObj.sendmail(sender, receivers, message.as_string())
    
    print("邮件发送成功")
    #except smtplib.SMTPException:
        #print("Error: 无法发送邮件")


def export_reserve_tranaction(exp_date, exp_days):
    export_path = '/tmp'
    export_file = export_path + '/reserve_' + exp_date + '.xlsx'
    os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.ZHS16GBK'

    db = create_engine('oracle://vms4:kCBwzRW-a7XI_Z4U@101.200.36.104:1521/tfdb', echo=True)
    Session = sessionmaker(bind=db)
    session = Session()

    classlist = []
    for name, obj in inspect.getmembers(models):
        if inspect.isclass(obj):
            classlist.append((name.lower(), obj))

    classdict = dict(classlist)
    t_order_master = aliased(classdict['tordermaster'])
    t_wechat_user = aliased(classdict['twechatuser'])
    t_order_detail = aliased(classdict['torderdetail'])
    t_mt_vm = aliased(classdict['tmtvm'])

    def get_transaction(begin_time, end_time):
        # try:
        reserve_tranaction_list = session.query(t_order_master.order_seq,
                                                t_wechat_user.phone,
                                                t_order_master.c_time,
                                                t_order_detail.valid_time,
                                                t_order_detail.order_detail_id,
                                                t_order_detail.vendout_status,
                                                t_mt_vm.inner_code). \
            join(t_wechat_user, t_order_master.consignee_id == t_wechat_user.user_id). \
            join(t_order_detail, t_order_master.order_id == t_order_detail.order_id). \
            join(t_mt_vm, t_mt_vm.vm_id == t_order_master.vm_id). \
            filter(t_order_master.order_type == 'reserve'). \
            filter(t_order_master.c_time > begin_time). \
            filter(t_order_master.c_time <= end_time). \
            order_by(t_order_detail.valid_time).all()
        # filter(t_order_master.order_seq == '00000010001413460451518425769347' ). \
        # all()
        session.commit()
        # order_by(t_order_master.c_time,desc)

        # except NoResultFound as e:
        #     print(e)
        # else:
        return reserve_tranaction_list

    # begin_time = datetime.datetime.now().strftime('%Y-%m-%d') - datetime.timedelta(month=1)
    end_time = datetime.datetime.strptime(exp_date, '%Y-%m-%d')
    begin_time = datetime.datetime.strptime(exp_date, '%Y-%m-%d') - datetime.timedelta(days=exp_days)
    export_date_list = [
        (datetime.datetime.strptime(exp_date, '%Y-%m-%d') - datetime.timedelta(days=1)).strftime('%Y-%m-%d'), exp_date,
        (datetime.datetime.strptime(exp_date, '%Y-%m-%d') + datetime.timedelta(days=1)).strftime('%Y-%m-%d')]

    # export_date_list = [
    #     (datetime.datetime.strptime(exp_date, '%Y-%m-%d') - datetime.timedelta(days=1)).strftime('%Y-%m-%d'), exp_date,
    #     (datetime.datetime.strptime(exp_date, '%Y-%m-%d') + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
    #     (datetime.datetime.strptime(exp_date, '%Y-%m-%d') + datetime.timedelta(days=2)).strftime('%Y-%m-%d'),
    #     (datetime.datetime.strptime(exp_date, '%Y-%m-%d') + datetime.timedelta(days=3)).strftime('%Y-%m-%d'),
    #     (datetime.datetime.strptime(exp_date, '%Y-%m-%d') + datetime.timedelta(days=4)).strftime('%Y-%m-%d')]
    # row = get_transaction(begin_time,end_time)
    session.close()

    wb = Workbook()
    # ws = wb.active
    ws = wb.create_sheet(title="reserve_tranaction", index=0)
    row_title = ['流水号', '电话', '创建时间', '取货有效期', '订单编号', '出货状态', '售货机编号']
    ws.append(row_title)

    for order_seq, phone, ctime, vtime, order_detail_id, vendout_status, inner_code in get_transaction(begin_time,
                                                                                                       end_time):
        # print(order_seq, phone, ctime, vtime, order_detail_id, vendout_status)
        if vtime in export_date_list:

            if vendout_status == 0:
                vendout_status = '成功'
                ws.append([order_seq, phone, ctime, vtime, order_detail_id, vendout_status, inner_code])
            elif vendout_status in (-1, 1, 2, 3, 4, 5, 6, 7, 8, 9):
                vendout_status = '失败'
                ws.append([order_seq, phone, ctime, vtime, order_detail_id, vendout_status, inner_code])
            else:
                vendout_status = '未知'
                ws.append([order_seq, phone, ctime, vtime, order_detail_id, vendout_status, inner_code])

    wb.save(export_file)
    return export_file


if __name__ == '__main__':
    receivers = ['wangmm@kdxfilm.com>', 'wufengxw@kdxfilm.com']
    today = datetime.datetime.now().strftime('%Y-%m-%d')
    send_file = export_reserve_tranaction(today, 31)
    if os.path.isfile(send_file):
        sendmail(receivers, today, send_file)
        print('exp_success !~')
    else:
        print('导出的文件不存在！~')
